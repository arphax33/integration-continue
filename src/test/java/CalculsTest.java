import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CalculsTest {
    private Calculs calculs;


    @BeforeEach
    void setUp() {
        calculs = new Calculs(15, 3);
    }

    @Test
    void multiplier() {
        assertEquals(45, calculs.multiplier());
    }

    @Test
    void additionner() {
        assertEquals(18, calculs.additionner());
    }

    @Test
    void diviser() {
        assertEquals(5, calculs.diviser());
    }

    @Test
    void soustraire() {
        assertEquals(12, calculs.soustraire());
    }
}